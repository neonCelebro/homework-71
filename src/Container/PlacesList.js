import React, {Fragment} from 'react';
import { connect } from 'react-redux';
import {fetchPlacesRequest} from "../Store/action";
import {FlatList, Image, Modal, Text, View} from "react-native";

import placeStyle from './placeStyle';



class PlacesList extends React.Component{
    componentDidMount() {
        this.props.getItems();
        };

    onEndReachedHandler = () => {
        console.log('TEST');
        const params = '?count=25&after='.concat(this.props.after);
        console.log(params, 'PARAMS');
        this.props.getItems(params);
    }
    render(){

        return (
            <View>
                <FlatList
                    data={this.props.items}
                    onEndReached={this.onEndReachedHandler}
                    onEndReachedThreshold={0.1}
                    keyExtractor={(item, index) => index}
                    renderItem={({item}) => {
                        return (
                            <View style={placeStyle.box}>
                                <Image style={placeStyle.itemImage} source={{uri: item.data.thumbnail}}/>
                                <Text style={placeStyle.itemText}>{item.data.title}</Text>
                            </View>
                        )
                    }}
                />

            </View>

        )
    }
}


const mapStoreToProps = state => {
    return {
        items: state.items,
        loading: state.loading,
        after: state.after,
        error: state.error,
    }
};
const mapDispatchToProps = dispatch => {
    return {
        getItems: (param) => dispatch(fetchPlacesRequest(param)),
    }
};

export default connect(mapStoreToProps, mapDispatchToProps)(PlacesList);