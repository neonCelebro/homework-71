import axios from '../axios-info';

export const FETCH_REQUEST =  'FETCH_TASK_REQUEST';
export const FETCH_SUCCSES = 'FETCH_TASK_SUCCSES';
export const FETCH_ERROR = 'FETCH_TASK_ERROR';


const fetchRequest = () => {
	return {type: FETCH_REQUEST}
};

const fetchSuccsess = (items) => {
	return {type: FETCH_SUCCSES, items}
};

const fetchError = (err) => {
	return {type: FETCH_ERROR, err}
};


export const fetchPlacesRequest = (param = '') => {
	return dispatch => {
		dispatch(fetchRequest());
		axios.get(param).then(
			(response) => {dispatch(fetchSuccsess(response.data.data))},
			(error) => {dispatch(fetchError(error))}
		);
	}
};



