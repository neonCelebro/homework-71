import * as actions from './action';

const initialState = {
	items: [],
	after: null,
	loading: false,
	error: false,
};

const reducer = (state = initialState, action) =>{
	switch (action.type) {
		case actions.FETCH_REQUEST:
			return {
				...state,
                loading: true
			};
		case actions.FETCH_SUCCSES:
			return{
				...state,
				items: action.items.children,
				after: action.items.after,
				loading: false,
			};
		case actions.FETCH_ERROR:
			return {
				...state,
				error: action.error,
                loading: false,
			};
		default: return state;
	}
};

export default reducer;