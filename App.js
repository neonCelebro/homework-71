import React from 'react';
import {ListView, StyleSheet, Text, View} from 'react-native';
import { Provider } from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import reducer from "./src/Store/reducer";
import PlacesList from "./src/Container/PlacesList";



const store = createStore(reducer, applyMiddleware(thunk));

export default class App extends React.Component {
  render() {
    return (
        <Provider store={store}>
            <View>
                <PlacesList/>
            </View>
        </Provider>
    );
  }
};




